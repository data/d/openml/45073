# OpenML dataset: eye_movements

https://www.openml.org/d/45073

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Jarkko Salojarvi, Kai Puolamaki, Jaana Simola, Lauri Kovanen, Ilpo Kojo, Samuel Kaski. Inferring Relevance from Eye Movements: Feature Extraction. Helsinki University of Technology, Publications in Computer and Information Science, Report A82. 3 March 2005. Data set at http://www.cis.hut.fi/eyechallenge2005/Competition 1 (preprocessed data) A straight-forward classification task. We provide pre-computed feature vectors for each word in the eye movement trajectory, with class labels.The dataset consist of several assignments. Each assignment consists of a question followed by ten sentences (titles of news articles). One of the sentences is the correct answer to the question (C) and five of the sentences are irrelevant to the question (I). Four of the sentences are relevant to the question (R), but they do not answer it.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45073) of an [OpenML dataset](https://www.openml.org/d/45073). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45073/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45073/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45073/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

